const calcExpr = (expr) => {
  const args = expr.split(' ');
  const newExpr = `${args[1]} ${args[0]} ${args[2]}`;
  const result = eval(newExpr);
  return eval(result);
};

const simplifyExpr = (expr) => {
  const regex = /\W -*\d+ -*\d+/;

  let simplifiable = expr.match(regex);
  let replacement;

  if (simplifiable === null) {
    let result = eval(expr);
    return result;
  } else {
    simplifiable = simplifiable[0];
    replacement = calcExpr(simplifiable);
  }

  expr = expr.replace(simplifiable, replacement);
  expr = simplifyExpr(expr);

  return expr;
};

exports.calculate = function(expression) {
  let result = simplifyExpr(expression);
  return result;
}

// console.log(exports.calculate('0'));
// console.log(exports.calculate('+ 3 4'));
// console.log(exports.calculate('- 3 * 4 5'));
// console.log(exports.calculate('* + 3 4 5'));
// console.log(exports.calculate('/ - 3 4 + 5 2'));
